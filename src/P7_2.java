import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Created by SANG on 10/7/15.
 */
public class P7_2 {

    static int Row = 1;

    public static void writeLine(String line, PrintWriter fileOut) {

        int i = 1;
        for (i = 1; i < line.length(); i++) {
            fileOut.println(String.format("/*" + i + "*/", Row, line));
            Row += 1;
        }
    }

    public static void main(String[] args){

        Scanner file = new Scanner(System.in);
        System.out.print("filename to read: ");
        String filenameRead = file.next();
        System.out.print("filename to write: ");
        String filenameWrite = file.next();

    }
}

