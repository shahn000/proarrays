/**
 * Created by SANG on 10/7/15.
 */
public class P6_23 {

    public static void histogram(int[] input) {

        int max = find_max(input);

        for (int i = 0; i < input.length; i++) {
            int time = (input[i] / max) * 40;
            for (int j = 0; j < time; j++) {

                System.out.print("*");
            }
        }
    }

    public static int find_max(int[] input) {

        int max = input[0];
        for (int i = 1; i < input.length; i++) {
            if (input[i] > max) {
                max = input[i];
            }
        }
        return max;
    }
}

