/**
 * Created by SANG on 10/7/15.
 */
public class P5_8 {

    public static String scramble(String word) {

        if (3 < word.length()) {

            int first = (int) (Math.random() * (word.length() - 3)) + 1;
            System.out.println(first);
            int last = (int) (Math.random() * (word.length() - first - 2)) + first + 1;
            System.out.println(last);

            if (last >= word.length() - 1) {

                System.out.println(first + " " + last);

            }
            return word.substring(0, first) + word.charAt(last) + word.substring(first + 1, last)
                    + word.charAt(first) + word.substring(last + 1);
        }
        else {
            return word;
        }
    }

    public static void main(String[] args) {
        System.out.println(scramble("hello"));
    }
}

