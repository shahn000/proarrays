/**
 * Created by SANG on 10/7/15.
 */
public class P5_20 {

    public static boolean isLeapYear(int year) {

        if (year % 400 == 0) {
            return true;
        }

        else if (year % 4 == 0 && year % 100 != 0) {
            return true;
        }

        return false;
    }

    public static void main(String[] args) {

        System.out.println(isLeapYear(2000));
        System.out.println(isLeapYear(2001));
        System.out.println(isLeapYear(2002));
        System.out.println(isLeapYear(2003));
        System.out.println(isLeapYear(2004));

    }
}

