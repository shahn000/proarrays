import java.util.Scanner;

/**
 * Created by SANG on 10/7/15.
 */
public class P5_25 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.print("Zip Code:");

        int zip_code = input.nextInt();
        int barcode = printDigit(zip_code);

        String start = "|";
        start = printBarcode(barcode) + start;
        for (int i = 0; i < 5; i++) {
            start = printBarcode(barcode) + start;
            zip_code /= 10;
        }
        start = "|" + start;
        System.out.println(start);
    }

    public static int printDigit(int zip) {

        int value = zip;
        int sum = 0;

        while (value > 0) {
            sum += value % 10;
            value /= 10;
        }

        return 10 - (sum % 10);
    }

    public static String printBarcode(int digit) {

        if (digit == 1) {
            return ":::||";
        }

        if (digit == 2) {
            return "::|:|";
        }

        if (digit == 3) {
            return "::||:";
        }

        if (digit == 4) {
            return ":|::|";
        }

        if (digit == 5) {
            return ":|:|:";
        }

        if (digit == 6) {
            return ":||::";
        }

        if (digit == 7) {
            return "|:::|";
        }

        if (digit == 8) {
            return "|::|:";
        }

        if (digit == 9) {
            return "|:|::";
        }

        return "||:::";
    }
}