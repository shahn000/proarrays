/**
 * Created by SANG on 10/7/15.
 */
public class P6_12 {

    public static void Run(int[] dice) {

        boolean inRun = false;
        int pre_dice = dice[0];

        for (int i = 0; i < dice.length - 1; i++) {
            if (inRun) {
                if (dice[i] != pre_dice) {
                    inRun = false;
                    System.out.print(")");
                }

            } else {
                if (dice[i] == dice[i + 1]) {
                    inRun = true;
                    System.out.print("(");
                }
            }
            pre_dice = dice[i];
            System.out.print(dice[i]);
        }

        if (inRun && dice[dice.length - 1] == pre_dice) {
            System.out.print(dice[dice.length - 1] + ")");

        } else if (inRun && dice[dice.length - 1] != pre_dice) {
            System.out.print(")" + dice[dice.length - 1]);

        } else {
            System.out.print(dice[dice.length - 1]);
        }
    }

    public static int[] dice_toss(int n) {

        int[] toss = new int[n];
        for (int i = 0; i < n; i++) {
            toss[i] = (int) (Math.random() * 6 + 1);
        }
        return toss;
    }
}
